from rest_framework import generics
from .models import User
from .serializers import UserSerializer


class UserGetAPI(generics.ListAPIView):
    serializer_class = UserSerializer

    def get_queryset(self):
        pk = self.kwargs['pk']
        return User.objects.filter(pk=pk)
