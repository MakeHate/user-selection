from user_selection.models import User
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string


class Command(BaseCommand):
    def handle(self, *args, **options) -> None:
        User.objects.create(
            username=get_random_string(10),
            avatar='avatars/admin.png',
            role_choice='1',
            offer=True
        )
        User.objects.create(
            username=get_random_string(10),
            avatar='avatars/user.png',
            role_choice='2',
            offer=True
        )
        User.objects.create(
            username=get_random_string(10),
            avatar='avatars/crm.png',
            role_choice='3',
            offer=True
        )
