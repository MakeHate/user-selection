from django.urls import path
from .views import UserGetAPI

app_name = 'user_selection'
urlpatterns = [
    path('api/v1/users/<int:pk>/', UserGetAPI.as_view(), name='UsersGetAPI')
]
