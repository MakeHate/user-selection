from rest_framework import serializers
from .models import User


class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(max_length=150)
    password = None
    role_choice = serializers.CharField(source='get_role_choice_display')
    avatar = serializers.ImageField()
    offer = serializers.BooleanField(read_only=True)

    class Meta:
        model = User
        fields = ['username', 'role_choice', 'avatar', 'offer']
