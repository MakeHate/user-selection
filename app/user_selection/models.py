from django.db.models import CharField, ImageField, BooleanField
from django.contrib.auth.models import AbstractBaseUser


class User(AbstractBaseUser):
    CHOICES = (
        ('1', 'Пользователь'),
        ('2', 'Менеджер'),
        ('3', 'CRM-администратор')
    )
    
    username = CharField(max_length=150, unique=True)
    password = None
    role_choice = CharField(max_length=200, choices=CHOICES)
    avatar = ImageField(upload_to='avatars/', null=True, blank=True)
    offer = BooleanField(default=True)
    
    USERNAME_FIELD = 'username'
