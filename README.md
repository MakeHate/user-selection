# User Selection
Test task from Compass Pro LLC.

## Clone this repository

```
git clone https://gitlab.com/MakeHate/user-selection.git
cd user_selection
cd app
```

## Start this app in docker and create superuser

```
sudo docker-compose run app django-admin startproject app .
sudo docker-compose run app python manage.py createsuperuser
```

## Make db migrations
```
sudo docker-compose run app python manage.py migrate
sudo docker-compose run app python manage.py makemigrations user_selection 
sudo docker-compose run app python manage.py migrate
```

## Up container
```
sudo docker-compose up 
```

## How use command
```
cd user_selection
cd user_selection
python manage.py create_users
```

# What problems have arisen
- There are problems with migration, for some reason it was not possible to make the migration happen immediately, but the command for its execution is in docker-compose. Also, migration for some reason happens with only one application.

# Offtop
- This is my first experience with Doсker and docker-compose. There was no similar experience (only with VirtualBox from Oracle, maybe), I had to study this technology very quickly. It wasn't easy, but I'm glad it turned out, although not in the way I expected.
